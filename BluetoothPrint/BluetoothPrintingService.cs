using Android.App;
using Android.Bluetooth;
using System.Linq;
using Android.Content;
using System.Threading.Tasks;
using Android.Graphics.Drawables;
using System.IO;
using Android.Graphics;
using Splat;
using BluetoothPrint.PCL;
using System.Collections;

namespace BluetoothPrint
{
	public class BluetoothPrintingService
	{
		private static readonly string SPPUUID = "00001101-0000-1000-8000-00805F9B34FB";
		private static readonly string myUUID = "f847c7ff-fc57-4f70-b4b0-3252cec0aff1";

		// based on this ESC/POS commands  http://content.epson.de/fileadmin/content/files/RSD/downloads/escpos.pdf
		// ECS/POS http://download.delfi.com/SupportDL/Epson/Manuals/TM-T88IV/Programming%20manual%20APG_1005_receipt.pdf


		private ImagePrintingService service;
		private BluetoothSocket socket;
		private BluetoothAdapter _adapter;
		private Context _context;

		public BluetoothPrintingService(Activity context)
		{
			_context = context;
			_adapter = BluetoothAdapter.DefaultAdapter;
		}

		/// <summary>
		/// True if device are connected
		/// </summary>
		public bool IsConnected
		{
			get { return socket != null && socket.IsConnected; }
		}

		/// <summary>
		/// Convert, prepare and print IBitmap to printer socket
		/// </summary>
		public void PrintImage(IBitmap bitmap)
		{
			if (!IsConnected)
				return;

			service.WriteHeader();

			var imageBitmap = ConvertIBitmapToBytes(bitmap);

			// To monochrome array of bits
			var bitmapData = ToMonochromeBitmap(imageBitmap);

			// writes array to output stream
			service.WriteRawBitmapData(bitmapData);

			service.WriteFooter();
		}
		/// <summary>
		/// Close stream, finish discovery and disable Bluetooth 
		/// </summary>
		public void OnDestroy()
		{
			CancelDiscovery();


			if (_adapter.IsEnabled)
				_adapter.Disable();

			if (service != null)
				service.CloseStream();
		}

		/// <summary>
		/// Enable Bluetooth and start devices discovery
		/// </summary>
		public void StartScanning()
		{
			if (!_adapter.IsEnabled)
			{
				_adapter.Enable();
			}
			// this is required to prevent erros in discovering
			CancelDiscovery();

			_adapter.StartDiscovery();
		}
		/// <summary>
		/// Connects to bluetooth device using MAC address
		/// </summary>
		public void ConnectToDevice(string deviceMac)
		{
			var device = _adapter.GetRemoteDevice(deviceMac);
			// if not paired, initialize pair process.
			if (device.BondState == Bond.None)
			{
				device.CreateBond();
			}

			var uuid = Java.Util.UUID.FromString(SPPUUID); //device.GetUuids().ElementAt(0).Uuid;

			if (device.BondState == Bond.Bonded)
			{
				if (IsConnected)
					return;

				try
				{
					// creating socket for sending data to printer
					socket = device.CreateRfcommSocketToServiceRecord(uuid);
				}
				catch (Java.IO.IOException ex)
				{
				}
				// this is required to call (to ensure no active dicsover), to prevent exception in Connect
				CancelDiscovery();
				socket.Connect();

				service = new ImagePrintingService(socket.OutputStream);
			}
		}


		/// <summary>
		/// Converts IBitmap to android Bitmap
		/// </summary>
		private Bitmap ConvertIBitmapToBytes(IBitmap bitmapDraw)
		{
			return ((BitmapDrawable)bitmapDraw.ToNative()).Bitmap;	
		}
				
		private BitmapData ToMonochromeBitmap(Bitmap bitmap)
		{
			var threshold = 127;
			var index = 0;
			var dimensions = bitmap.Width * bitmap.Height;
			var dots = new BitArray(dimensions);

			for (var y = 0; y < bitmap.Height; y++)
			{
				for (var x = 0; x < bitmap.Width; x++)
				{
					var color = new Color(bitmap.GetPixel(x, y));
					// calculate relative luminance
					var luminance = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
					dots[index] = (luminance < threshold);  // true if need to draw bit, false otherwise
					index++;
				}
			}

			return new BitmapData()
			{
				Dots = dots,
				Height = bitmap.Height,
				Width = bitmap.Width
			};

		}

		private void CancelDiscovery()
		{
			if (_adapter.IsDiscovering)
				_adapter.CancelDiscovery();
		}
	}
}