﻿using System;
using Android.Content;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Platform;
using MvvmCross.Platform;

namespace BluetoothPrint
{
	/// <summary>
	/// Setup logic required by MvvmCross
	/// </summary>
	public class Setup : MvxAndroidSetup
	{
		public Setup(Context applicationContext) : base(applicationContext)
		{
		}

		protected override IMvxApplication CreateApp()
		{
			return new BluetoothPrint.PCL.App();
		}
	}
}
