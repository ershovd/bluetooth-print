﻿using Android.App;
using BluetoothPrint.PCL;
using Android.Bluetooth;
using Android.Content;
using Android.Widget;

namespace BluetoothPrint
{
	/// <summary>
	/// Main application activity.
	/// </summary>
	[Activity(Label = "Bluetooth Print", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : BaseActivity<MainViewmodel>
	{
		private BluetoothReceiver receive;
		private BluetoothPrintingService service;

		protected override void OnViewModelSet()
		{
			base.OnViewModelSet();
			SetContentView(Resource.Layout.Main);

			InitializeReciever();
			// subscribe for printing click in ViewModel
			ViewModel.SelectedBluetoothDeviceEvent += OnSelectedDeviceHandler;

			service = new BluetoothPrintingService(this);
			service.StartScanning();
		}

		/// <summary>
		/// Create BluetoothReceiver and initialize necessary IntentFilter
		/// </summary>
		private void InitializeReciever()
		{
			receive = new BluetoothReceiver(this.ViewModel);
			var filter = new IntentFilter(BluetoothDevice.ActionFound);
			RegisterReceiver(receive, filter);
			filter = new IntentFilter(BluetoothAdapter.ActionDiscoveryFinished);
			RegisterReceiver(receive, filter);
		}

		async void OnSelectedDeviceHandler(object sender, BluetoothDeviceEventArgs e)
		{
			try
			{
				// show "Loading.." progress dialog 
				ViewModel.IsBusy = true;

				// establish connection to selected Bluetooth device
				service.ConnectToDevice(e.Device.MAC);

				// load image from resource
				var bitmapDrawable = await ViewModel.DownloadImageBitmap("Icon.png");
				// converts and print image
				service.PrintImage(bitmapDrawable);

				// hide "Loading.." progress dialog
				ViewModel.IsBusy = false;
			}
			catch (System.Exception ex)
			{
				var toast = new Toast(this);
				toast.SetText(ex.ToString());
				toast.Show();
			}
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			service.OnDestroy();
			UnregisterReceiver(receive);
		}
	}
}