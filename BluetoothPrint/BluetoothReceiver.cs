using BluetoothPrint.PCL;
using Android.Bluetooth;
using System.Linq;
using Android.Content;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using Models = BluetoothPrint.PCL.Models;

namespace BluetoothPrint
{
	/// <summary>
	/// Reciever implementation for gettting broadcast on bluetooth devices found and discovery ended
	/// </summary>
	public class BluetoothReceiver : BroadcastReceiver
	{
		MainViewmodel _viewModel;
		private List<BluetoothDevice> _devices;

		public BluetoothReceiver(MainViewmodel viewModel)
		{
			_viewModel = viewModel;
			_devices = new List<BluetoothDevice>();
		}

		public override void OnReceive(Context context, Intent intent)
		{
			string action = intent.Action;

			if (action == BluetoothDevice.ActionFound)
			{
				// Show "Loading..." progress dialog
				_viewModel.IsBusy = true;
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
				if (!_devices.Any(f => f.Address == device.Address))
				{
					_devices.Add(device);
				}
			}
			else if (action == BluetoothAdapter.ActionDiscoveryFinished)
			{
				_viewModel.Devices = new MvxObservableCollection<Models.BluetoothDevice>
					(
						_devices.Select(f => new Models.BluetoothDevice(f.Address, f.Name, (Models.BondState)f.BondState))
					);

				// hide "Loading..." progress dialog 
				_viewModel.IsBusy = false;
			}
		}
	}
}
