﻿using System;
using Android.OS;
using BluetoothPrint.PCL;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Views;

namespace BluetoothPrint
{
	/// <summary>
	/// Base activity for binding ProgressDialog to MvxViewmodel
	/// </summary>
	public abstract class BaseActivity<TViewModel> : MvxAppCompatActivity<TViewModel>
	where TViewModel : BaseViewModel
	{
		private BindableProgress _bindableProgress;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			_bindableProgress = new BindableProgress(this);
			this.CreateBindingSet<BaseActivity<TViewModel>, TViewModel>()
				.Bind(_bindableProgress).For(p => p.Visible).To(vm => vm.IsBusy)
				.Apply();
		}
	}
}
