﻿using System;
using Android.App;
using Android.Content;

namespace BluetoothPrint
{
	/// <summary>
	/// Support class for binding ProgressDialog visiblity to Viemodel properties
	/// </summary>
	public class BindableProgress
	{
		private readonly Context _context;
		private ProgressDialog _progressDialog;

		public BindableProgress(Context context)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}

			_context = context;
		}

		/// <summary>
		/// If need to display indefiinte progress dialog
		/// </summary>
		public bool Visible
		{
			get { return _progressDialog != null; }
			set
			{
				if (value == (_progressDialog != null))
				{
					return;
				}

				if (value)
				{

					_progressDialog = ProgressDialog.Show(_context, "Loading...", null, true);

				}
				else
				{
					_progressDialog.Hide();
					_progressDialog = null;
				}
			}
		}
	}
}
