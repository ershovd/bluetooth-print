﻿using System;
using MvvmCross.Core.ViewModels;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Main entry point for the application (used by MvvmCross).
	/// </summary>
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			RegisterAppStart<MainViewmodel>();
		}
	}
}
