﻿using MvvmCross.Core.ViewModels;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Base view model for handling busy indicator status
	/// </summary>
	public abstract class BaseViewModel : MvxViewModel
	{
		private bool _isBusy;

		public bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				if (_isBusy == value)
				{
					return;
				}

				_isBusy = value;
				RaisePropertyChanged(() => IsBusy);
				OnIsBusyChanged(IsBusy);
			}
		}

		protected virtual void OnIsBusyChanged(bool isBusy)
		{
		}
	}
}
