﻿using System;
using System.Collections;
using System.Threading.Tasks;
using BluetoothPrint.PCL.Models;
using MvvmCross.Core.ViewModels;
using Splat;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Core logic class for handling printing commands and bluetooth devices
	/// </summary>
	public class MainViewmodel : BaseViewModel
	{
		public MainViewmodel()
		{
			Devices = new MvxObservableCollection<BluetoothDevice>();
		}

		private MvxCommand _printCommand;
		/// <summary>
		/// Run the printig
		/// </summary>
		public MvxCommand PrintCommand
		{
			get { return _printCommand ?? (_printCommand = new MvxCommand(PrintCommandInternal)); }
		}

		private void PrintCommandInternal()
		{
			OnSelectedBluetoothDevice(SelectedDevice);
		}

		private MvxObservableCollection<BluetoothDevice> _devices;
		/// <summary>
		/// List of available Bluetooth devices
		/// </summary>
		public MvxObservableCollection<BluetoothDevice> Devices
		{
			get { return _devices; }
			set
			{
				_devices = value;
				RaisePropertyChanged(() => Devices);
				RaisePropertyChanged(() => NoDevicesFound);
			}
		}

		private BluetoothDevice _selectedDevice;
		/// <summary>
		/// Currently selected Bluetooth device
		/// </summary>
		public BluetoothDevice SelectedDevice
		{
			get { return _selectedDevice; }
			set
			{
				_selectedDevice = value;
				RaisePropertyChanged(() => SelectedDevice);
				RaisePropertyChanged(() => IsPrintEnabled);
			}
		}

		/// <summary>
		/// Is printing enabled
		/// </summary>
		/// <value><c>true</c> if is print enabled; otherwise, <c>false</c>.</value>
		public bool IsPrintEnabled
		{
			get { return SelectedDevice != null; }
		}

		/// <summary>
		/// True if no Bluetooth devices found.
		/// </summary>
		public bool NoDevicesFound
		{
			get { return Devices == null || Devices.Count == 0; }
		}
		/// <summary>
		/// Occurs when device is selected from list.
		/// </summary>
		public event EventHandler<BluetoothDeviceEventArgs> SelectedBluetoothDeviceEvent;
		private void OnSelectedBluetoothDevice(BluetoothDevice device)
		{
			var invoke = SelectedBluetoothDeviceEvent;
			invoke?.Invoke(this, new BluetoothDeviceEventArgs(device));
		}

		/// <summary>
		/// Asyncronously download an image with specified param name="imageName" from resources and returns IBitmap
		/// </summary>
		public async Task<IBitmap> DownloadImageBitmap(string imageName)
		{
			return await BitmapLoader.Current.LoadFromResource(imageName, null, null);
		}
	}
}