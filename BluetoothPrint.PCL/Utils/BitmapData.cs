using System.Collections;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Heper class for bit array of dots
	/// </summary>
	public class BitmapData
	{
		public int Height { get; set; }
		public int Width { get; set; }
		public BitArray Dots { get; set; }
	}
}