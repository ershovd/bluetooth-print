using System;
using BluetoothPrint.PCL.Models;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Event arguments with Bluetooth device
	/// </summary>
	public class BluetoothDeviceEventArgs : EventArgs
	{
		public BluetoothDeviceEventArgs(BluetoothDevice device)
		{
			this.Device = device;
		}
		/// <summary>
		/// Bluetooth device information
		/// </summary>
		public BluetoothDevice Device { get; private set; }
	}
}
