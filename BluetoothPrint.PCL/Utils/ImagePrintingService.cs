using System;
using System.Threading.Tasks;
using BluetoothPrint.PCL.Models;
using MvvmCross.Core.ViewModels;
using Splat;

namespace BluetoothPrint.PCL
{
	/// <summary>
	/// Service class for converting, preparing and printing image to stream
	/// </summary>
	public class ImagePrintingService
	{
		private System.IO.Stream _outputStream;

		public ImagePrintingService(System.IO.Stream stream)
		{
			_outputStream = stream;
		}

		private static byte[] SELECT_BIT_IMAGE_MODE = { 0x1B, 0x2A, 33 };
		private static byte[] FEED_LINE = { 10 };
		private static byte[] ESC_INIT = { 27, 64 };
		private static byte LINE_SPACE = 24;
		private static byte[] SET_LINE_SPACING_24 = { 0x1B, 0x33, LINE_SPACE };

		/// <summary>
		/// Writes monochomoe pixel bitmap data to output stream
		/// </summary>
		/// <remarks>based on http://toanbily.blogspot.ru/2010/04/sending-bit-image-to-epson-tm-t88iii.html </remarks>
		public void WriteRawBitmapData(BitmapData data)
		{
			// used https://stackoverflow.com/questions/14099239/printing-a-bit-map-image-to-pos-printer-via-comport-in-c-sharp/14099717#14099717

			byte[] width = BitConverter.GetBytes(data.Width); // get Width value as byte array

			int offset = 0;
			while (offset < data.Height)
			{
				WriteToStream(SELECT_BIT_IMAGE_MODE);

				WriteToStream(width[0]);  // width low byte
				WriteToStream(width[1]);  // width high byte

				for (int x = 0; x < data.Width; ++x)
				{

					// Remember, 24 dots = 24 bits = 3 bytes. 
					// The 'k' variable keeps track of which of those
					// three bytes that we're currently scribbling into.
					for (int k = 0; k < 3; ++k)
					{
						byte slice = 0;

						// A byte is 8 bits. The 'b' variable keeps track
						// of which bit in the byte we're recording.                 
						for (int b = 0; b < 8; ++b)
						{

							// Calculate the y position that we're currently
							// trying to draw. We take our offset, divide it
							// by 8 so we're talking about the y offset in
							// terms of bytes, add our current 'k' byte
							// offset to that, multiple by 8 to get it in terms
							// of bits again, and add our bit offset to it.

							int y = (((offset / 8) + k) * 8) + b;

							// Calculate the location of the pixel we want in the bit array.

							int i = (y * data.Width) + x;

							// If the image (or this stripe of the image)
							// is shorter than 24 dots, pad with zero.

							bool v = false;

							if (i < data.Dots.Length)
							{
								v = data.Dots[i];
							}

							// Finally, store our bit in the byte that we're currently
							// scribbling to. Our current 'b' is actually the exact
							// opposite of where we want it to be in the byte, so
							// subtract it from 7, shift our bit into place in a temp
							// byte, and OR it with the target byte to get it into there.

							slice |= (byte)((v ? 1 : 0) << (7 - b));
						}

						WriteToStream(slice);
					}
				}

				// We're done with this 24-dot high pass. Render a newline
				// to bump the print head down to the next line

				offset += (int)LINE_SPACE;
				WriteToStream(FEED_LINE);
			}
		}

		private void WriteToStream(byte oneByte)
		{
			WriteToStream(new byte[] { oneByte });
		}

		private void WriteToStream(byte[] bytes)
		{
			_outputStream.Write(bytes, 0, bytes.Length);
		}

		/// <summary>
		/// Writes bytes for finalize printing 
		/// </summary>
		public void WriteFooter()
		{
			WriteToStream(FEED_LINE);
			_outputStream.Flush();
		}

		/// <summary>
		/// Writes bytes to initialize printing
		/// </summary>
		public void WriteHeader()
		{
			// init printer
			WriteToStream(ESC_INIT);
            // set line spacing
			WriteToStream(SET_LINE_SPACING_24);
		}

		/// <summary>
		/// Closes the stream
		/// </summary>
		public void CloseStream()
		{
			if (_outputStream != null)
			{
				_outputStream.Dispose();  //.Close();
			}
		}
	}
}
