using System;
namespace BluetoothPrint.PCL.Models
{
	/// <summary>
	/// Bluetooth device info
	/// </summary>
	public class BluetoothDevice
	{
		public BluetoothDevice(string mac, string name, BondState bondState)
		{
			MAC = mac;
			Name = name;
			BondState = bondState;
		}

		public string MAC { get; set; }
		public string Name { get;  set; }
		public BondState BondState { get; private set; }
	}
}
