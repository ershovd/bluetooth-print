﻿using System;
namespace BluetoothPrint.PCL.Models
{
	public enum BondState
	{
		Bonded = 12,
		Bonding = 11,
		None = 10
	}
}
